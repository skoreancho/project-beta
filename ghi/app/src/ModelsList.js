import React, { useState, useEffect } from 'react'

function ModelsList() {
    const [models, setModels] = useState([]);

    const getData = async () => {
        const response = await fetch("http://localhost:8100/api/models/");

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(() => {
        getData()
    }, [])


    return (
        <>
        <h1>Models</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {models.map(model => {
                    return (
                        <tr key={model.id}>
                            <td>{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td><img src={model.picture_url} alt={model.name} width='100' height='100'></img></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        <div>
            <a href='http://localhost:3000/models/new/'><button type='button'>New Model</button></a>
        </div>
        </>
    );
}

export default ModelsList;
