import React, {useEffect, useState} from 'react';

function SalespersonHistory(props) {
    const [salesperson, setSalesperson] = useState('');
    const [salespersons, setSalespersons] = useState([]);
    const [sales, setSales] = useState([]);

    const handleSalespersonChange = async event => {
        const value = event.target.value;
        setSalesperson(value);
    }


    const fetchData = async () => {
        const salespersonUrl = "http://localhost:8090/api/salespersons/";
        const saleUrl = "http://localhost:8090/api/sales/";

        const salespersonResponse = await fetch(salespersonUrl);
        const saleResponse = await fetch(saleUrl)

        if (salespersonResponse.ok) {
            const salespersonData = await salespersonResponse.json();
            setSalespersons(salespersonData.salespersons);
        }

        if (saleResponse.ok) {
            const saleData = await saleResponse.json();
            setSales(saleData.sales);
        }

    }

    useEffect(() => {
        fetchData();
      }, []);

    const selectedSales = sales.filter((sale) => sale.salesperson.number === parseInt(salesperson));

    return(
        <div className="mb-3">
            <div className=" py-4 text-left">
                <h1>Salesperson history</h1>
            </div>
            <select onChange={handleSalespersonChange} value={salesperson} required id="salesperson" name="salesperson" className="form-select" >
                <option value="">Choose a salesperson</option>
                {salespersons.map( salesperson => {
                return (
                    <option key={salesperson.number} value={salesperson.number}>
                    {salesperson.name}
                    </option>
                )
                })}
            </select>
            <p></p>
            <table className="table table-striped">
                <thead>
                  <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Sale price</th>
                  </tr>
                </thead>
                <tbody>
                  {selectedSales.map(sale => {
                    return (
                      <tr key={sale.id}>
                        <td>{ sale.salesperson.name }</td>
                        <td>{ sale.customer.name }</td>
                        <td>{ sale.automobile.vin }</td>
                        <td>{ sale.price }</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
        </div>
    )
}

export default SalespersonHistory;
