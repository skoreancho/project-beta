import React, {useEffect, useState} from 'react';

function SaleForm() {
    const [automobile, setAutomobile] = useState('');
    const [automobiles, setAutomobiles] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [salespersons, setSalespersons] = useState([]);
    const [customer, setCustomer] = useState('');
    const [customers, setCustomers] = useState([]);
    const [price, setPrice] = useState('');

    const handleAutomobileChange = event => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handleSalespersonChange = event => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleCustomerChange = event => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handlePriceChange = event => {
        const value = event.target.value;
        setPrice(value);
    }


    const handleSubmit = async event => {
        event.preventDefault();

        const data = {};

        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;


        const url = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
        }

        const updatedAutomobilesUrl = "http://localhost:8090/api/automobiles/";
        const updatedAutomobilesResponse = await fetch(updatedAutomobilesUrl);
        const updatedAutomobilesData = await updatedAutomobilesResponse.json();
        setAutomobiles(updatedAutomobilesData.automobiles);
    };

    const fetchData = async () => {
        const automobilesUrl = "http://localhost:8090/api/automobiles/";
        const salespersonUrl = "http://localhost:8090/api/salespersons/";
        const customerUrl = "http://localhost:8090/api/customers/";

        const automobilesResponse = await fetch(automobilesUrl);
        const salespersonResponse = await fetch(salespersonUrl);
        const customerResponse = await fetch(customerUrl);

        if (automobilesResponse.ok) {
            const automobilesData = await automobilesResponse.json();
            setAutomobiles(automobilesData.automobiles);
        }

        if (salespersonResponse.ok) {
            const salespersonData = await salespersonResponse.json();
            setSalespersons(salespersonData.salespersons);
        }

        if (customerResponse.ok) {
            const customerData = await customerResponse.json();
            setCustomers(customerData.customers);
        }
    }

    useEffect(() => {
      fetchData();
    }, []);

    const selectedAutomobiles = automobiles.filter(automobile => automobile.sold === false)

    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a sale record</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="mb-3">
                <select onChange={handleAutomobileChange} value={automobile} required id="automobile" name="automobile" className="form-select" >
                  <option value="">Choose an automobile</option>
                  {selectedAutomobiles.map( automobile => {
                    return (
                      <option key={automobile.vin} value={automobile.vin}>
                        {automobile.model} {automobile.year}
                      </option>
                    )
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={handleSalespersonChange} value={salesperson} required id="salesperson" name="salesperson" className="form-select" >
                  <option value="">Choose a salesperson</option>
                  {salespersons.map( salesperson => {
                    return (
                      <option key={salesperson.number} value={salesperson.number}>
                        {salesperson.name}
                      </option>
                    )
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={handleCustomerChange} value={customer} required id="customer" name="customer" className="form-select" >
                  <option value="">Choose a customer</option>
                  {customers.map( customer => {
                    return (
                      <option key={customer.phone_number} value={customer.phone_number}>
                        {customer.name}
                      </option>
                    )
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePriceChange} value={price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                <label htmlFor="price">Sale price</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default SaleForm;
