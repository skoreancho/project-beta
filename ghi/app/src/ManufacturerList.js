import React, {useState, useEffect} from 'react';

function ManufacturerList() {
  const [manufacturers, setManufacturers] = useState([]);

  const manufacturerList = async () => {
    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const manufacturerResponse = await fetch(manufacturerUrl);
    if (manufacturerResponse.ok) {
      const manufacturerData = await manufacturerResponse.json();
      setManufacturers(manufacturerData.manufacturers)
    }
  }

  useEffect(() => {
    manufacturerList();
  }, []);

  return (
  <>
    <div className=" py-4 text-left">
      <h1>Manufacturers</h1>
    </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {manufacturers.map(manufacturer => {
          return (
            <tr key={manufacturer.href}>
              <td>{ manufacturer.name }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  )
}

export default ManufacturerList;
