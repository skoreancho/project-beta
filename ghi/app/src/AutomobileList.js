import React, {useState, useEffect} from 'react';

function AutomobileList() {
  // const [automobile, setAutomobile] = useState('');
  const [automobiles, setAutomobiles] = useState([]);

  const automobileList = async () => {
    const automobileUrl = "http://localhost:8100/api/automobiles/";
    const automobileResponse = await fetch(automobileUrl);
    if (automobileResponse.ok) {
      const automobileData = await automobileResponse.json();
      setAutomobiles(automobileData.autos)
    }
  }

  useEffect(() => {
    automobileList();
  }, []);

  return (
    <>
      <div className=" py-4 text-left">
        <h1>Automobiles</h1>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map(automobile => {
            return (
              <tr key={automobile.href}>
                <td>{ automobile.vin }</td>
                <td>{ automobile.color }</td>
                <td>{ automobile.year }</td>
                <td>{ automobile.model.name }</td>
                <td>{ automobile.model.manufacturer.name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  )
}

export default AutomobileList;
