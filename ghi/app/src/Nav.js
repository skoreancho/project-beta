import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/models/">Vehicle Models</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/models/new/">Add a Vehicle Model</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/">Service Appointments</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/new/">Create a Service Appointment</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="appointments/history/">Service Appointment History</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/technicians/">Technicians</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/technicians/new/">Add a Technician</NavLink>
          </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/manufacturers/new">Create a manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/automobiles">Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/automobiles/new">Create an automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/salesperson/new">Add a sales person</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/customer/new">Add a customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/sale/new">Create a sale form</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/sale">Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/salesperson/history">Salesperson history</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
