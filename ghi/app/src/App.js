import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ModelForm from './ModelForm';
import ModelsList from './ModelsList';
import ServiceApptForm from './ServiceApptForm';
import ServiceApptsList from './ServiceApptsList';
import TechnicianForm from './TechnicianForm';
import TechniciansList from './TechniciansList';
import ServiceApptHistory from './ServiceApptHistory';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import SalespersonForm from './SalespersonForm';
import CustomerForm from './CustomerForm';
import SaleForm from './SaleForm';
import SaleList from './SaleList';
import SalespersonHistory from './SalespersonHistory';

function App(props) {
  // if (props.manufacturers === undefined) {
  //   return null;
  // }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="models/">
            <Route index element={<ModelsList />} />
            <Route path='new/' element={<ModelForm />} />
          </Route>
          <Route path="appointments/">
            <Route index element={<ServiceApptsList />} />
            <Route path="new/" element={<ServiceApptForm />} />
            <Route path="history/" element={<ServiceApptHistory />} />
          </Route>
          <Route path="technicians/">
            <Route index element={<TechniciansList />} />
            <Route path="new/" element={<TechnicianForm />} />
          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="automobiles">
            <Route path="" element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="salesperson">
            <Route path="new" element={<SalespersonForm />} />
            <Route path="history" element={<SalespersonHistory />} />
          </Route>
          <Route path="customer">
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="sale">
            <Route path="" element={<SaleList />} />
            <Route path="new" element={<SaleForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
