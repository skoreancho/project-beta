import React from 'react';

class ServiceApptForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vin: "",
            customer: "",
            date: "",
            time: "",
            reason: "",
            technician_id: "",
            technicians: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleCustomerChange = this.handleCustomerChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);
        this.handleTechnicianChange = this.handleTechnicianChange.bind(this);
    }

    async componentDidMount() {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const response = await fetch(technicianUrl);

        if (response.ok) {
            const data = await response.json();
            this.setState({ technicians: data.technicians });
        }
    }

    handleVinChange(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }

    handleCustomerChange(event) {
        const value = event.target.value;
        this.setState({ customer: value });
    }

    handleDateChange(event) {
        const value = event.target.value;
        this.setState({ date: value });
    }

    handleTimeChange(event) {
        const value = event.target.value;
        this.setState({ time: value });
    }

    handleReasonChange(event) {
        const value = event.target.value;
        this.setState({ reason: value });
    }

    handleTechnicianChange(event) {
        const value = event.target.value;
        this.setState({ technician_id: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.technicians;

        const appointmentsUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(appointmentsUrl, fetchConfig);
        if (response.ok) {
            this.setState(
                {
                    vin: "",
                    customer: "",
                    date: "",
                    time: "",
                    reason: "",
                    technician_id: "",
                }
            );
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-model-form">
                            <div className="form-floating mb-3">
                                <input
                                    value={this.state.vin}
                                    onChange={this.handleVinChange}
                                    placeholder="VIN"
                                    required
                                    type="text"
                                    name="vin"
                                    id="vin"
                                    className="form-control"
                                />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    value={this.state.customer}
                                    onChange={this.handleCustomerChange}
                                    placeholder="Customer"
                                    required
                                    type="text"
                                    name="customer"
                                    id="customer"
                                    className="form-control"
                                />
                                <label htmlFor="customer">Customer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    value={this.state.date}
                                    onChange={this.handleDateChange}
                                    placeholder="Date"
                                    required
                                    type="text"
                                    name="date"
                                    id="date"
                                    className="form-control"
                                />
                                <label htmlFor="date">Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    value={this.state.time}
                                    onChange={this.handleTimeChange}
                                    placeholder="Time"
                                    required
                                    type="text"
                                    name="time"
                                    id="time"
                                    className="form-control"
                                />
                                <label htmlFor="time">Time</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    value={this.state.reason}
                                    onChange={this.handleReasonChange}
                                    placeholder="Reason"
                                    required
                                    type="text"
                                    name="reason"
                                    id="reason"
                                    className="form-control"
                                />
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    value={this.state.technician_id}
                                    onChange={this.handleTechnicianChange}
                                    required
                                    name="technician"
                                    id="technician"
                                    className="form-select"
                                >
                                    <option value="">Choose a technician</option>
                                    {this.state.technicians.map(technician => {
                                        return (
                                            <option key={technician.id} value={technician.id}>
                                                {technician.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                    <div>
                        <a href='http://localhost:3000/appointments/'><button type='button'>Appointments</button></a>
                    </div>
                </div>
            </div>
        );
    }
}

export default ServiceApptForm;
