from django.shortcuts import render
from .models import Appointment, Technician, AutomobileVO
from django.views.decorators.http import require_http_methods
from django.core.serializers.json import DjangoJSONEncoder
from common.json import ModelEncoder
from django.http import JsonResponse
import json
from datetime import date, time

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "import_href",
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_num",
    ]

@require_http_methods(["GET","POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET","DELETE"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            count, _ = Technician.objects.filter(id=pk).delete()
            return JsonResponse({"delete": count > 0})

        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET","POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all().values()
        return JsonResponse({"appointments": list(appointments)})
    else:
        content = json.loads(request.body)
        try:
            technician_id = content["technician_id"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid technician"}, status=400)

        appointment = Appointment.objects.create(**content)
        appointment_dict = {
            "id": appointment.id,
            "vin": appointment.vin,
            "customer": appointment.customer,
            "date": appointment.date,
            "time": appointment.time,
            "reason": appointment.reason,
            "vip": appointment.vip,
            "finished": appointment.finished,
            "technician": appointment.technician,
        }
        return JsonResponse(appointment_dict)





@require_http_methods(["GET","DELETE", "PUT"])
def api_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment_dict = {
                "id": appointment.id,
                "vin": appointment.vin,
                "customer": appointment.customer,
                "date": appointment.date,
                "time": appointment.time,
                "reason": appointment.reason,
                "vip": appointment.vip,
                "finished": appointment.finished,
                "technician": appointment.technician,
            }
            if appointment.technician:
                appointment_dict["technician"] = json.dumps(appointment.technician.name, encoder=TechnicianEncoder)
            else:
                appointment_dict["technician"] = None
            return JsonResponse(appointment_dict)
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Appointment.objects.filter(id=pk).delete()
            return JsonResponse({"delete": count > 0})

        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            Appointment.objects.filter(id=id).update(**content)
            appt = Appointment.objects.get(id=id)
            appointment_dict = {
                "id": appointment.id,
                "vin": appointment.vin,
                "customer": appointment.customer,
                "date": appointment.date,
                "time": appointment.time,
                "reason": appointment.reason,
                "vip": appointment.vip,
                "finished": appointment.finished,
                "technician": appointment.technician,
            }
            if appointment.technician:
                appointment_dict["technician"] = json.dumps(appointment.technician.name, encoder=TechnicianEncoder)
            else:
                appointment_dict["technician"] = None
            return JsonResponse(appointment_dict)
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response



@require_http_methods(["GET"])
def api_appt_history(request, vin):
    if request.method == "GET":
        try:
            vin = Appointment.objects.all().values()
            return JsonResponse(
                {"vin": list(vin)}
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Vin does not exist"},
                status=400,
            )
