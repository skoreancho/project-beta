from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Customer, Sale


# Create your views here.

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "name",
        "number",
        "id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id",
    ]

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "color",
        "year",
        "vin",
        "model",
        "manufacturer",
        "sold",
    ]

    # def get_extra_data(self, o):
    #     return {
    #         "model": o.model.name,
    #         "manufacturer": o.model.manufacturer.name
    #     }

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "automobile",
        "salesperson",
        "customer",
        "price",
        "id",
    ]
    encoders={
        "automobile": AutomobileVODetailEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_automobiles(request):
    if request.method == "GET":
        automobile = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobile},
            encoder = AutomobileVODetailEncoder,
        )
    else:
        content = json.loads(request.body)
        automobile = AutomobileVO.objects.create(**content)
        return JsonResponse(
            automobile,
            encoder=AutomobileVODetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": salesperson},
            encoder = SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

# @require_http_methods(["GET", "DELETE", "PUT"])
# def api_show_salesperson(request):
#     if request.method == "GET:
#     else:

@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customers": customer},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        return JsonResponse(
            Customer.objects.create(**content),
            encoder=CustomerEncoder,
            safe=False,
        )

# @require_http_methods(["GET", "DELETE", "PUT"])
# def api_show_customer(request):
#     if request.method == "GET:
#     else:

@require_http_methods(["GET", "POST"])
def api_list_sale(request):
    if request.method == "GET":
        sale = Sale.objects.all()
        return JsonResponse(
            {"sales": sale},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            automobile.sold = True
            automobile.save()
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile"},
                status=400,
            )
        try:
            salesperson = Salesperson.objects.get(number=content["salesperson"])
            content['salesperson'] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson"},
                status=400,
            )
        try:
            customer = Customer.objects.get(phone_number=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer"},
                status=400,
            )
        return JsonResponse(
            Sale.objects.create(**content),
            encoder=SaleEncoder,
            safe=False,
        )

# @require_http_methods(["GET", "DELETE", "PUT"])
# def api_show_sale(request, pk):
#     if request.method == "GET:
#     else:
