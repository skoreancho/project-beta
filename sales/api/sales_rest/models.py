from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    color = models.CharField(max_length=200)
    year = models.PositiveIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    model = models.CharField(max_length=200)
    manufacturer = models.CharField(max_length=200)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

class Salesperson(models.Model):
    name = models.CharField(max_length=200)
    number = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.id

class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=50)
    phone_number = models.PositiveIntegerField()

    def __str__(self):
        return self.id

class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name = "sales",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name= "sales",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name= "sales",
        on_delete=models.CASCADE,
    )
    price = models.PositiveIntegerField()
